'use strict';

(function(angular) {
    var app = angular.module('supersonic');

    app.config([
        'DEBUG',
        'MODULES',
        '$ocLazyLoadProvider',
        function(
            DEBUG,
            MODULES,
            $ocLazyLoadProvider
        ) {
            var process = function(modules) {
                angular.forEach(modules, function(val, idx) {
                    val.cache = ! DEBUG;
                });
                return modules;
            };

            $ocLazyLoadProvider.config({
                debug: DEBUG,
                modules: process(MODULES)
            });
        }
    ])
})(angular);
