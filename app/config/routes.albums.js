'use strict';

(function(angular) {
    var app = angular.module('supersonic.routes');

    app.config([
        '$stateProvider',
        function(
            $stateProvider
        ) {

            $stateProvider
                .state('app.albums', {
                    title: 'Albums',
                    templateUrl: 'app/templates/albums/list.html',
                    controller: 'AlbumsCtrl',
                    url: '/albums',
                    resolve: {
                        loadCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.albums',
                            ]);
                        }],
                    }
                })

                .state('app.album', {
                    title: 'Album',
                    templateUrl: 'app/templates/albums/main.html',
                    controller: 'AlbumCtrl',
                    url: '/album/:id',
                    resolve: {
                        loadCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.albums',
                            ]);
                        }],
                        album: ['SubsonicAPI', '$stateParams', function(SubsonicAPI, $stateParams) {
                            return SubsonicAPI.getAlbum($stateParams.id);
                        }]
                    }
                })

        }
    ]);

})(angular);
