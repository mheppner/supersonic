'use strict';

(function(angular) {
    var app = angular.module('supersonic.routes');

    app.config([
        '$stateProvider',
        function(
            $stateProvider
        ) {

            $stateProvider
                .state('app.artists', {
                    abstract: true,
                    templateUrl: 'app/templates/artists/base.html',
                    controller: 'ArtistsBaseCtrl',
                    url: '/artists',
                    resolve: {
                        loadCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.artists',
                            ]);
                        }],
                        indexes: ['SubsonicAPI', function(SubsonicAPI) {
                            return SubsonicAPI.getArtists();
                        }],
                    }
                })

                .state('app.artists.blank', {
                    url: '',
                    controller: 'ArtistsBlankCtrl'
                })

                .state('app.artists.list', {
                    title: 'Artists Index',
                    url: '/:index',
                    templateUrl: 'app/templates/artists/list.html',
                    controller: 'ArtistsCtrl',
                    resolve: {
                        artists: [
                            '$filter',
                            '$stateParams',
                            'indexes',
                            function(
                                $filter,
                                $stateParams,
                                indexes
                            ) {
                                var found = $filter('filter')(indexes.data.artists.index, {name: $stateParams.index});

                                if (! found.length) {
                                    // TODO handle 404
                                    return;
                                }

                                return found[0].artist;
                            }
                        ]
                    }
                })

                .state('app.artist', {
                    title: 'Artist',
                    url: '/artist/:id',
                    templateUrl: 'app/templates/artists/main.html',
                    controller: 'ArtistCtrl',
                    resolve: {
                        loadCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.artists',
                            ]);
                        }],
                        artist: [
                            '$stateParams',
                            'SubsonicAPI',
                            function(
                                $stateParams,
                                SubsonicAPI
                            ) {
                                return SubsonicAPI.getArtist($stateParams.id);
                            }
                        ],
                        artistDetails: [
                            '$stateParams',
                            'SubsonicAPI',
                            function(
                                $stateParams,
                                SubsonicAPI
                            ) {
                                return SubsonicAPI.getArtistInfo2($stateParams.id);
                            }
                        ],
                        lastFMDetails: [
                            'resolveApp',
                            'LastFM',
                            'artist',
                            function(
                                resolveApp,
                                LastFM,
                                artist
                            ) {
                                return LastFM.artist.getInfo(artist.data.artist.name);
                            }
                        ]
                    }
                })

        }
    ]);

})(angular);
