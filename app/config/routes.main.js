'use strict';

(function(angular) {
    var app = angular.module('supersonic.routes');

    app.run([
        '$log',
        '$rootScope',
        'AuthSrvc',
        function(
            $log,
            $rootScope,
            AuthSrvc
        ) {
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                $log.info('$stateChangeStart', toState, toParams, fromState, fromParams);

                // consider the app to be unauthenticated
                if (toState.name.indexOf('app.') === 0 && AuthSrvc.isGuest()) {
                    event.preventDefault();

                    $rootScope.addAlert({
                        text: 'You need to be authenticated to access this page.'
                    });
                    $rootScope.$state.go('login');
                }
            });

            $rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams){
                $log.info('$stateNotFound', unfoundState, fromState, fromParams);
            });

            $rootScope.$on('$stateChangeError', function(event, toParams, fromState, fromParams, error) {
                $log.error('$stateChangeError', toParams, fromState, fromParams, error);
            });
        }
    ]);


    app.config([
        '$stateProvider',
        '$urlRouterProvider',
        function(
            $stateProvider,
            $urlRouterProvider
        ) {
            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('app', {
                    abstract: true,
                    templateUrl: 'app/templates/base/app.html',
                    controller: 'AppCtrl',
                    resolve: {
                        resolveApp: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.app',
                                'supersonic.controllers.sidebar',
                                'supersonic.services.lastfm',
                            ]);
                        }],
                    }
                })

                .state('app.home', {
                    title: 'Home',
                    url: '/',
                    templateUrl: 'app/templates/home/main.html',
                    controller: 'HomeCtrl',
                    resolve: {
                        loadCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.home',
                            ]);
                        }]
                    }
                })

                .state('app.settings', {
                    title: 'Settings',
                    url: '/settings',
                    templateUrl: 'app/templates/auth/settings.html',
                    controller: 'SettingsCtrl',
                })

                .state('login', {
                    title: 'Login',
                    url: '/login',
                    templateUrl: 'app/templates/auth/login.html',
                    controller: 'LoginCtrl',
                    resolve: {
                        loadCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.auth',
                            ]);
                        }]
                    }
                })

                .state('logout', {
                    title: 'Logout',
                    url: '/logout',
                    templateUrl: 'app/templates/auth/logout.html',
                    controller: 'LogoutCtrl',
                    resolve: {
                        loadCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'supersonic.controllers.auth',
                            ]);
                        }]
                    }
                })
        }
    ]);

})(angular);
