'use strict';

(function(angular) {
    var app = angular.module('supersonic.controllers.albums', ['supersonic']);

    app.controller('AlbumsCtrl', [
        '$scope',
        'SubsonicAPI',
        function(
            $scope,
            SubsonicAPI
        ) {
            var pageSize = 50; // TODO move constant?

            // tabs to display
            $scope.tabs = [
                { key: 'random', name: 'Random' },
                { key: 'newest', name: 'Newest' },
                { key: 'frequent', name: 'Frequent' },
                { key: 'starred', name: 'Starred' },
            ];

            // set the initial active tab
            $scope.tabActive = {};
            $scope.tabActive[$scope.view.albumsPage] = true;

            // pager offset
            $scope.offset = 0;

            // flag to disable links
            $scope.loading = false;

            // updates the albums using the scope variables
            $scope.updateView = function() {
                $scope.loading = true;

                SubsonicAPI.getAlbumList2(
                    $scope.view.albumsPage,
                    pageSize,
                    $scope.offset
                ).then(function(data) {
                    $scope.albums = data.data.albumList2.album;
                    $scope.loading = false;
                }, function(data) {
                    $scope.loading = false;
                });
            };

            // pager to increment offset and update albums
            $scope.pageNext = function() {
                $scope.offset += 50;
                $scope.updateView();
            };

            // pager to decrement offset and update albums
            $scope.pagePrevious = function() {
                $scope.offset = Math.max(0, $scope.offset - 50);
                $scope.updateView();
            };

            // update the albums when the tab changes
            $scope.$watch('view.albumsPage', function() {
                $scope.offset = 0;
                $scope.updateView();
            });
        }
    ]);


    app.controller('AlbumCtrl', [
        '$scope',
        'album',
        function(
            $scope,
            album
        ) {
            $scope.album = album.data.album;
            $scope.trackList = 'app/templates/albums/trackList.html';
        }
    ]);
})(angular);
