'use strict';

(function(angular) {
    var app = angular.module('supersonic.controllers.main', ['supersonic']);

    app.controller('AppCtrl', [
        '$scope',
        'PlayerSrvc',
        function(
            $scope,
            PlayerSrvc
        ) {
            $scope.Player = PlayerSrvc;
        }
    ]);


    app.controller('SettingsCtrl', [
        '$scope',
        function(
            $scope
        ) {

        }
    ]);


})(angular);
