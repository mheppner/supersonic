'use strict';

(function(angular){

    var app = angular.module('supersonic.controllers.artists', ['supersonic']);

    app.controller('ArtistsBaseCtrl',[
        '$scope',
        'indexes',
        function(
            $scope,
            indexes
        ) {
            $scope.indexes = indexes.data.artists.index;
        }
    ]);

    app.controller('ArtistsBlankCtrl', [
        '$filter',
        '$scope',
        'localStorageService',
        function(
            $filter,
            $scope,
            localStorageService
        ) {
            var lastIndex = localStorageService.get('lastArtistIndex'),

                // go to the first available index
                goToFirst = function() {
                    if ($scope.indexes.length) {
                        $scope.$state.go('app.artists.list', {
                            index: $scope.indexes[0].name
                        });
                    }
                };


            if (lastIndex) {
                // a last index was found in the cache, try to find it from the local results
                var found = $filter('filter')($scope.indexes, {name: lastIndex});

                if (found.length) {
                    // found an index that matches the one in the cache, go there
                    $scope.$state.go('app.artists.list', {
                        index: found[0].name
                    });
                } else {
                    // none matching, go to the first index
                    goToFirst();
                }
            } else {
                // nothing in the cache, go to the first index
                goToFirst();
            }
        }
    ])

    app.controller('ArtistsCtrl', [
        '$scope',
        '$stateParams',
        'localStorageService',
        'SubsonicAPI',
        'artists',
        function(
            $scope,
            $stateParams,
            localStorageService,
            SubsonicAPI,
            artists
        ) {
            var image;

            // save our current index for later - used by blank state
            localStorageService.set('lastArtistIndex', $stateParams.index);

            // save the filtered artists from this state's resolve
            $scope.artists = artists;

            angular.forEach($scope.artists, function(artist, idx) {
                // for each artist, fetch the lastm info
                SubsonicAPI.getArtistInfo2(artist.id).then(function(data) {
                    // save the details
                    artist.details = data.data.artistInfo2;
                });

            });
        }
    ]);


    app.controller('ArtistCtrl', [
        '$filter',
        '$scope',
        'artist',
        'artistDetails',
        'lastFMDetails',
        'SubsonicAPI',
        function(
            $filter,
            $scope,
            artist,
            artistDetails,
            lastFMDetails,
            SubsonicAPI
        ) {
            $scope.trackList = 'app/templates/albums/trackList.html';
            $scope.artist = artist.data.artist;
            $scope.albums = $filter('orderBy')( $scope.artist.album, '-year');
            $scope.artistDetails = artistDetails.data.artistInfo2;
            $scope.lastFM = lastFMDetails.data.artist;

            // set the active tab
            $scope.tabActive = {};
            $scope.tabActive[$scope.view.artistPage] = true;

            // for each album, fetch the album details
            // TODO cache this?
            angular.forEach($scope.albums, function(album, idx) {
                SubsonicAPI.getAlbum(album.id).then(function(data) {
                    // replace the album with the newly fetched data
                    $scope.albums[idx] = data.data.album;
                }, function(data) {

                });
            });
        }
    ]);

})(angular)
