'use strict';

(function(angular) {
    var app = angular.module('supersonic.controllers.auth', ['supersonic']);

    app.controller('LoginCtrl', [
        '$scope',
        '$http',
        '$rootScope',
        'SubsonicAPI',
        'SubsonicPassword',
        function(
            $scope,
            $http,
            $rootScope,
            SubsonicAPI,
            SubsonicPassword
        ) {
            $scope.loading = false;
            $scope.password = '';

            /**
             * Action to submit the login form. Basically just calls the
             * getUser() endpoint and checks if the request succeeded. If
             * successful, the state is transitioned. If not, a message is
             * displayed and nothing happens.
             */
            $scope.submit = function() {
                $scope.loading = true;

                SubsonicAPI.getUser($scope.server.username)
                    .then(function(data) {
                        $scope.loading = false;

                        if (data.data.status == 'ok') {
                            $rootScope.user = data.data.user;
                            $scope.$state.go('app.home');
                        }
                    }, function(data) {
                        $scope.loading = false;
                        $scope.addAlert({
                            type: 'danger',
                            text: 'The server could not be reached!'
                        });
                    });
            };

            /**
             * The onChange callback for the password field. The internal password
             * on the scope is converted to hex and stored in local storage.
             */
            $scope.hexPassword = function() {
                $scope.server.password = SubsonicPassword.hex($scope.password);
            };
        }
    ]);


    app.controller('LogoutCtrl', [
        'AuthSrvc',
        function(
            AuthSrvc
        ) {
            AuthSrvc.logout();
        }
    ]);

})(angular);
