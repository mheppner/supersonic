'use strict';

(function(angular) {
    var app = angular.module('supersonic.controllers');

    app.controller('NavbarCtrl', [
        '$scope',
        function(
            $scope
        ) {
            $scope.template = 'app/templates/base/navbar.html';
        }
    ]);

    app.controller('AlertsCtrl', [
        '$scope',
        '$filter',
        '$rootScope',
        '$timeout',
        function(
            $scope,
            $filter,
            $rootScope,
            $timeout
        ) {
            $scope.template = 'app/templates/base/alerts.html';
            $scope.alerts = [];

            /**
             * Remove an alert by filtering out those matching the given alert
             * creation time.
             * @param  {object} a an alert with property of added
             */
            $rootScope.removeAlert = function(a) {
                $scope.alerts = $filter('filter')($scope.alerts, {'added': '!' + a.added});
            };

            /**
             * Adds an alert with a creation time. If a type is not given, the
             * default time of 'info' will be used. A timeout will be fired
             * to automatically remove the alert.
             * @param  {object} a an alert with properties 'text' and optionally 'type'
             */
            $rootScope.addAlert = function(a) {
                // added value is used as a lookup key for removing
                a.added = new Date().getTime();

                // prefix the given type or provide a default type
                a.type = a.type ? 'alert-' + a.type : 'alert-info';

                // save alert
                $scope.alerts.push(a);

                // automatically remove the alert
                $timeout(function(){
                    $rootScope.removeAlert(a);
                }, 6500);
            }
        }
    ]);


})(angular);
