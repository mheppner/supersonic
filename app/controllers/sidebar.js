'use strict';

(function(angular) {
    var app = angular.module('supersonic.controllers.sidebar', ['supersonic']);

    app.controller('SidebarCtrl', [
        '$scope',
        function(
            $scope
        ) {
            $scope.template = 'app/templates/sidebar/main.html';
        }
    ]);


})(angular);
