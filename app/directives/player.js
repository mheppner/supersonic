'use strict';

(function(angular) {
    var app = angular.module('supersonic.directives');

    app.directive('supersonicPlayer', [
        '$rootScope',
        '$interval',
        'PlayerSrvc',
        function(
            $rootScope,
            $interval,
            PlayerSrvc
        ) {
            return {
                restrict: 'EAC',
                scope:

                },
                templateUrl: 'app/templates/directives/player.html',
                link: function($scope, $el, attrs) {
                    $scope.Player = PlayerSrvc;
                    $scope.volume = 100;

                    var timer = $interval(function() {
                        // update the local percentage for the song
                        $scope.percent = $scope.Player.getPercent();
                    }, 250);

                    $scope.clickPosition = function(evt) {
                        var $parent = angular.element(evt.target).parent(),
                            song = $scope.Player.getSong(),
                            percent;

                        if (song) {
                            // there is an active song, so get the click position percentage
                            percent = evt.offsetX / $parent.width() * 100;

                            // set the percentage of where to play
                            song.sound.setPercent(percent);
                        }
                    };

                    $scope.clickVolume = function(evt) {
                        var $parent = angular.element(evt.target).parent();

                        // get the click percentage and set the volume
                        $scope.volume = evt.offsetX / $parent.width() * 100;
                        $scope.Player.setVolume($scope.volume);
                    };

                    $scope.$on('$destroy', function() {
                        if (angular.isDefined(timer)) {
                            // destroy the timer, if any
                            $interval.cancel(timer);
                            timer = undefined;
                        }
                    });

                }
            };
        }
    ]);

})(angular);
