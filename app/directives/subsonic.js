'use strict';

(function(angular) {
    var app = angular.module('supersonic.directives');

    app.directive('subsonicImg', [
        '$rootScope',
        'SubsonicAPI',
        function(
            $rootScope,
            SubsonicAPI
        ) {
            return {
                restrict: 'EAC',
                scope: {
                    coverArtId: '=',
                    avatar: '='
                },
                template: '<img data-ng-src="{{ fullUrl }}" class="img-responsive">',
                link: function($scope, $el, attrs) {
                    if ($scope.avatar) {
                        $scope.fullUrl = SubsonicAPI.getAvatar($rootScope.user.username);
                    } else {
                        $scope.fullUrl = SubsonicAPI.getCoverArt($scope.coverArtId);
                    }
                }
            };
        }
    ]);


    app.directive('gridImg', [
        'SubsonicAPI',
        function(
            SubsonicAPI
        ) {

            return {
                restrict: 'EAC',
                scope: {
                    imgSrc: '=',
                    contents: '=',
                    subsonicId: '='
                },
                template: ' \
                    <div class="background" data-ng-style="{\'background-image\': \'url(\'+ imgSrc  + \')\'}"></div> \
                    <div class="content"> \
                        <div class="table"> \
                            <div class="cell text-center"> \
                                {{ contents }} \
                            </div> \
                        </div> \
                    </div> \
                ',
                link: function($scope, $el, attrs) {
                    if ($scope.subsonicId) {
                        $scope.imgSrc = SubsonicAPI.getCoverArt($scope.subsonicId);
                    }
                }
            };
        }
    ]);


})(angular);
