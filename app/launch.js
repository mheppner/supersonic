'use strict';

(function(angular) {

    angular.module('supersonic.controllers', []);
    angular.module('supersonic.services', []);
    angular.module('supersonic.directives', []);
    angular.module('supersonic.routes', []);

    var app = angular.module('supersonic', [
        'ngAnimate',
        'ngResource',
        'ngTouch',
        'ngCookies',

        'oc.lazyLoad',
        'ui.router',
        'ui.bootstrap',
        'LocalStorageModule',
        'angular-cache',

        'supersonic.routes',
        'supersonic.services',
        'supersonic.controllers',
        'supersonic.directives',
    ]);


    app.run([
        '$rootScope',
        '$state',
        'localStorageService',
        'AuthSrvc',
        'CacheFactory',
        function(
            $rootScope,
            $state,
            localStorageService,
            AuthSrvc,
            CacheFactory
        ) {
            // used to create cache buckets
            var cacheDefinitions = {
                subsonic: 'sub',
                lastFM: 'lfm'
            };

            // global app variables
            $rootScope.$state = $state;
            $rootScope.Auth = AuthSrvc;
            $rootScope.caches = {};

            // set or create cache buckets and add them to $rootScope
            angular.forEach(cacheDefinitions, function(val, idx) {
                $rootScope.caches[idx] = CacheFactory.get(val);

                if (! $rootScope.caches[idx]) {
                    $rootScope.caches[idx] = CacheFactory.createCache(val);
                }
            });

            // bind local storage objects to the root scope
            localStorageService.bind($rootScope, 'server', {
                url: '',
                username: '',
                password: '',
            });
            localStorageService.bind($rootScope, 'user', {});
            localStorageService.bind($rootScope, 'view', {
                mode: 'grid',
                aristPage: 'albums',
                sidebar: 'browse',
                albumsPage: 'random',
            });

        }
    ]);


    app.config([
        '$httpProvider',
        '$resourceProvider',
        'CacheFactoryProvider',
        'localStorageServiceProvider',
        function(
            $httpProvider,
            $resourceProvider,
            CacheFactoryProvider,
            localStorageServiceProvider
        ) {
            //$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
            $resourceProvider.defaults.stripTrailingSlashes = false;

            localStorageServiceProvider.setPrefix('supersonic');

            angular.extend(CacheFactoryProvider.defaults, {
                storageMode: 'localStorage',
                storagePrefix: 'supson.c.', // "supersonic.cache."
            });
        }
    ]);


    // TODO move this once there are other filters
    app.filter("sanitize", [
        '$sce',
        function(
            $sce
        ) {
            // provides a filter to allow html content from a variable
            return function(htmlCode){
                return $sce.trustAsHtml(htmlCode);
            }
        }
    ]);

})(angular);
