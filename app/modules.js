'use strict';

(function(angular) {
    var app = angular.module('supersonic');

    app.constant('MODULES', [
        {
            name: 'supersonic.controllers.app',
            files: [
                'app/controllers/app.js'
            ]
        }, {
            name: 'supersonic.controllers.sidebar',
            files: [
                'app/controllers/sidebar.js'
            ]
        }, {
            name: 'supersonic.controllers.navbar',
            files: [
                'app/controllers/navbar.js'
            ]
        }, {
            name: 'supersonic.controllers.auth',
            files: [
                'app/controllers/auth.js'
            ]
        }, {
            name: 'supersonic.controllers.home',
            files: [
                'app/controllers/home.js'
            ]
        }, {
            name: 'supersonic.controllers.artists',
            files: [
                'app/controllers/artists.js'
            ]
        }, {
            name: 'supersonic.controllers.albums',
            files: [
                'app/controllers/albums.js'
            ]
        }, {
            name: 'supersonic.services.lastfm',
            files: [
                'app/services/lastfm.js'
            ]
        }
    ]);

})(angular);
