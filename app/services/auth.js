'use strict';

(function(angular) {
    var app = angular.module('supersonic.services');

    app.factory('AuthSrvc', [
        '$rootScope',
        'CacheFactory',
        function(
            $rootScope,
            CacheFactory
        ) {
            return {
                isAuthenticated: function() {
                    return $rootScope.server &&
                        $rootScope.server.url &&
                        $rootScope.server.username &&
                        $rootScope.user &&
                        $rootScope.user.username;
                },
                isGuest: function() {
                    return ! this.isAuthenticated();
                },
                logout: function() {
                    // clear the user and server
                    // this should also remove it from localstorage
                    $rootScope.server = {};
                    $rootScope.user = {};

                    // clear out all of the cache buckets
                    CacheFactory.clearAll();
                }
            }
        }
    ]);

})(angular);
