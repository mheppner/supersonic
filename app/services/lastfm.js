'use strict';

(function(angular, jQuery) {
    var app = angular.module('supersonic.services.lastfm', ['supersonic']);

    app.factory('LastFM', [
        '$rootScope',
        '$http',
        '$q',
        'localStorageService',
        'LASTFM_KEY',
        function(
            $rootScope,
            $http,
            $q,
            localStorageService,
            LASTFM_KEY
        ) {
            var cache = $rootScope.caches.lastFM;

            /**
             * Adds a transform callback to the $http response defaults.
             * @param  {function} transform a callback that will be appended to defaults
             * @return {Array}           the response callbacks
             */
            var appendTransform = function(transform) {
                // we can't guarantee that the default transformation is an array
                var defaults = $http.defaults.transformResponse;
                defaults = angular.isArray(defaults) ? defaults : [defaults];

                // append the new transformation to the defaults
                return defaults.concat(transform);
            };

            /**
             * A general function to call the Subsonic API. The server information
             * should already be stored in the local storage (username,
             * password, and url). An $http request will be created. The
             * response will be transformed to match the Subsonic API specs.
             * @param  {string} path   the URL path after /rest/
             * @param  {object} params GET params
             * @return {$promise}      the $http promise object
             */
            var call = function(path, params) {
                var // params for the request
                    defaultParams = {
                        api_key: LASTFM_KEY,
                        format: 'json',
                        method: path,
                    },
                    // params used for the cache key
                    cacheKeyParams = {
                        method: path,
                    },
                    cacheKey,
                    deferred = $q.defer();

                // the cache key is the selected params above
                cacheKey = jQuery.param(angular.extend(cacheKeyParams, params));

                if (cache.get(cacheKey)) {
                    // already have the key, can resolve with the contents
                    var found = cache.get(cacheKey);
                    deferred.resolve({data: found});
                } else {
                    $http({
                        url: 'http://ws.audioscrobbler.com/2.0/',
                        method: 'GET',
                        params: angular.extend(defaultParams, params),
                        transformResponse: appendTransform(function(data) {
                            // only allow successful requests to be put into the cache
                            if ('error' in data && data['error']) {
                                // response failed, add an alert
                                $rootScope.addAlert({
                                    text: 'LastFM: ' + data['message'],
                                    type: 'danger'
                                });
                            } else {
                                // response was successful, add the data to the cache
                                cache.put(cacheKey, data);
                            }

                            return data;
                        })
                    }).then(function(data) {
                            deferred.resolve(data);
                        }, function(data) {
                            deferred.reject(data);
                        });
                }

                return deferred.promise;
            };


            return {
                // TODO only add routes we need
                artist: {
                    getInfo: function(artist, mbid, lang) {
                        return call('artist.getInfo', {
                            artist: artist,
                            mbid: mbid,
                            lang: lang,
                            autocorrect: 1,
                        });
                    }
                }
            }

        }
    ]);

})(angular, jQuery);
