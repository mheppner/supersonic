'use strict';

(function(angular, jQuery) {
    var app = angular.module('supersonic.services');

    app.factory('SubsonicPassword', [
        function() {
            return {
                /**
                 * Provides a helper function to convert a plain-text password
                 * into a hex-encoded password.
                 * @param  {string} plain the plain-text password
                 * @return {string}       the hex encoded password
                 */
                hex: function(plain) {
                    var hex = '',
                        i;

                    if (plain) {
                        for (i = 0; i < plain.length; i++) {
                            hex += plain.charCodeAt(i).toString(16);
                        }
                    }

                    return hex;
                }
            };
        }
    ]);


    app.factory('SubsonicAPI', [
        '$rootScope',
        '$http',
        '$q',
        'localStorageService',
        function(
            $rootScope,
            $http,
            $q,
            localStorageService
        ) {
            var cache = $rootScope.caches.subsonic;

            /**
             * Adds a transform callback to the $http response defaults.
             * @param  {function} transform a callback that will be appended to defaults
             * @return {Array}           the response callbacks
             */
            var appendTransform = function(transform) {
                // we can't guarantee that the default transformation is an array
                var defaults = $http.defaults.transformResponse;
                defaults = angular.isArray(defaults) ? defaults : [defaults];

                // append the new transformation to the defaults
                return defaults.concat(transform);
            };

            /**
             * Create a URL from the server's base URL and the given endpoint.
             * @param  {string} path The endpoint of the API to call
             * @return {string}      The absolute URL to the endpoint
             */
            var buildUrl = function(path) {
                var server = localStorageService.get('server');

                // make sure the server url has a trailing slash
                if (server.url.slice(-1) !== '/') {
                    server.url += '/';
                }

                return server.url + 'rest/' + path;
            };

            /**
             * Construct GET parameters with default values needed to call all
             * API endpoints. The params passed in will override defaults.
             * @param  {object} params The GET params to extend off of the defaults
             * @return {object}        A merged object
             */
            var getParams = function(params) {
                var server = localStorageService.get('server'),
                    defaultParams = {
                        u: server.username,
                        p: 'enc:' + server.password,
                        f: 'json',
                        v: '1.0.1',
                        c: 'Supersonic',
                    };

                return angular.extend(defaultParams, params);
            };

            /**
             * A general function to call the Subsonic API. The server information
             * should already be stored in the local storage (username,
             * password, and url). An $http request will be created. The
             * response will be transformed to match the Subsonic API specs.
             * @param  {string} path   the URL path after /rest/
             * @param  {object} params GET params
             * @return {$promise}      the $http promise object
             */
            var call = function(path, params, useCache) {
                var cacheKey = '',
                    deferred = $q.defer();

                // if using the cache, set a key
                if (useCache) {
                    cacheKey = buildUrl(path) + '?' + jQuery.param(getParams(params));
                }

                if (cache.get(cacheKey)) {
                    // already have the key, can resolve with the contents
                    // if not using the cache, an empty string will return an undefined cache bucket
                    var found = cache.get(cacheKey);
                    deferred.resolve({data: found});
                } else {
                    // don't have cache, fetch the resource
                    $http({
                        url: buildUrl(path),
                        method: 'GET',
                        params: getParams(params),
                        transformResponse: appendTransform(function(value) {
                            if (angular.isObject(value) && 'subsonic-response' in value) {
                                var data = value['subsonic-response'];

                                if (data['status'] == 'failed') {
                                    // if the response failed, add an alert
                                    $rootScope.addAlert({
                                        text: data['error']['message'],
                                        type: 'danger'
                                    });
                                } else {
                                    // response was a success
                                    if (useCache) {
                                        cache.put(cacheKey, data);
                                    }
                                }
                                return data;
                            }
                            return value;
                        })
                    }).then(function(data) {
                            deferred.resolve(data);
                        }, (function(data) {
                            deferred.reject(data);
                        })
                    );
                }

                return deferred.promise;
            };


            return {
                ping: function() {
                    return call('ping.view');
                },
                getLicense: function() {
                    return call('getLicense.view');
                },
                getMusicFolders: function() {
                    return call('getMusicFolders.view');
                },
                getIndexes: function(musicFolderId, ifModifiedSince) {
                    return call('getIndexes.view', {
                        musicFolderId: musicFolderId,
                        ifModifiedSince: ifModifiedSince
                    });
                },
                getMusicDirectory: function(id) {
                    return call('getMusicDirectory.view', {
                        id: id
                    });
                },
                getGenres: function() {
                    return call('getGenres.view');
                },
                getArtists: function() {
                    return call('getArtists.view');
                },
                getArtist: function(id) {
                    return call('getArtist.view', {
                        id: id
                    });
                },
                getAlbum: function(id) {
                    return call('getAlbum.view', {
                        id: id
                    });
                },
                getSong: function(id) {
                    return call('getSong.view', {
                        id: id
                    });
                },
                getVideos: function() {
                    return call('getVideos.view');
                },
                getArtistInfo: function(id, count, includeNotPresent) {
                    return call('getArtistInfo.view', {
                        id: id,
                        count: count,
                        includeNotPresent: includeNotPresent
                    }, true);
                },
                getArtistInfo2: function(id, count, includeNotPresent) {
                    return call('getArtistInfo2.view', {
                        id: id,
                        count: count,
                        includeNotPresent: includeNotPresent
                    }, true);
                },
                getSimilarSongs: function(id, count) {
                    return call('getSimilarSongs.view', {
                        id: id,
                        count: count
                    });
                },
                getSimilarSongs2: function(id, count) {
                    return call('getSimilarSongs2.view', {
                        id: id,
                        count: count
                    });
                },
                getAlbumList: function(type, size, offset, fromYear, toYear, genre, musicFolderId) {
                    return call('getAlbumList.view', {
                        type: type,
                        size: size,
                        offset: offset,
                        fromYear: fromYear,
                        toYear: toYear,
                        genre: genre,
                        musicFolderId, musicFolderId
                    });
                },
                getAlbumList2: function(type, size, offset, fromYear, toYear, genre, musicFolderId) {
                    return call('getAlbumList2.view', {
                        type: type,
                        size: size,
                        offset: offset,
                        fromYear: fromYear,
                        toYear: toYear,
                        genre: genre,
                        musicFolderId, musicFolderId
                    });
                },
                getRandomSongs: function(size, genre, fromYear, toYear, musicFolderId) {
                    return call('getRandomSongs.view', {
                        size: size,
                        genre: genre,
                        fromYear: fromYear,
                        toYear: toYear,
                        musicFolderId: musicFolderId
                    });
                },
                getSongsByGenre: function(genre, count, offset, musicFolderId) {
                    return call('getSongsByGenre.view', {
                        genre: genre,
                        count: count,
                        offset: offset,
                        musicFolderId: musicFolderId
                    })
                },
                getNowPlaying: function() {
                    return call('getNowPlaying.view');
                },
                getStarred: function(musicFolderId) {
                    return call('getStarred.view', {
                        musicFolderId: musicFolderId
                    });
                },
                getStarred2: function(musicFolderId) {
                    return call('getStarred2.view', {
                        musicFolderId: musicFolderId
                    });
                },
                search2: function(query, artistCount, artistOffset, albumCount, albumOffset, songCount, songOffset, musicFolderId) {
                    return call('search2.view', {
                        query: query,
                        artistCount: artistCount,
                        artistOffset: artistOffset,
                        albumCount: albumCount,
                        albumOffset: albumOffset,
                        songCount: songCount,
                        songOffset: songOffset,
                        musicFolderId: musicFolderId
                    });
                },
                search3: function(query, artistCount, artistOffset, albumCount, albumOffset, songCount, songOffset, musicFolderId) {
                    return call('search2.view', {
                        query: query,
                        artistCount: artistCount,
                        artistOffset: artistOffset,
                        albumCount: albumCount,
                        albumOffset: albumOffset,
                        songCount: songCount,
                        songOffset: songOffset,
                        musicFolderId: musicFolderId
                    });
                },
                getPlaylists: function(username) {
                    return call('getPlaylists.view', {
                        username: username
                    });
                },
                getPlaylist: function(id) {
                    return call('getPlaylist.view', {
                        id: id
                    });
                },
                createPlaylist: function(playlistId, name, songId) {
                    return call('createPlaylist.view', {
                        playlistId: playlistId, // if updating
                        name: name,             // if new
                        songId: songId          // TODO can add multiple?
                    });
                },
                updatePlaylist: function(playlistId, name, comment, isPublic, songIdToAdd, songIndexToRemove) {
                    return call('updatePlaylist.view', {
                        playlistId: playlistId,
                        name: name,
                        comment: comment,
                        public: isPublic,
                        songIdToAdd: songIdToAdd, // TODO multiple
                        songIndexToRemove: songIndexToRemove //TODO multiple
                    })
                },
                deletePlaylist: function(id) {
                    return call('deletePlaylist.view', {
                        id: id
                    });
                },
                stream: function(id, maxBitRate, format, timeOffset, size, estimateContentLength) {
                    // required params
                    var params = {
                        id: id
                    };

                    // optional params
                    if (maxBitRate) {
                        params.maxBitRate = maxBitRate;
                    }
                    if (format) {
                        params.format = format;
                    }
                    if (timeOffset) {
                        params.timeOffset = timeOffset;
                    }
                    if (size) {
                        params.size = size;
                    }
                    if (estimateContentLength) {
                        params.estimateContentLength = estimateContentLength;
                    }

                    return buildUrl('stream.view?' + jQuery.param(getParams(params)));
                },
                download: function(id) {
                    // TODO handle download
                    return call('download.view', {
                        id: id
                    });
                },
                hls: function(id, bitRate) {
                    // TODO handle m3u8
                    return call('hls.m3u8', {
                        id: id,
                        bitRate: bitRate
                    })
                },
                getCoverArt: function(id, size) {
                    // required params
                    var params = {
                        id: id
                    };

                    // optional params
                    if (size) {
                        params.size = size;
                    }

                    // TODO we have to use jQuery because Angular doesn't expose method
                    // of how they convert an object to GET params.
                    return buildUrl('getCoverArt.view?' + jQuery.param(getParams(params)));
                },
                getLyrics: function(artist, title) {
                    return call('getLyrics.view', {
                        artist: artist,
                        title: title
                    });
                },
                getAvatar: function(username) {
                    var params = {
                        username: username
                    };

                    return buildUrl('getAvatar.view?' + jQuery.param(getParams(params)));
                },
                star: function(id, albumId, artistId) {
                    return call('star.view', {
                        id: id,
                        albumId: albumId,
                        artistId: artistId
                    });
                },
                unstar: function(id, albumId, artistId) {
                    return call('unstar.view', {
                        id: id,
                        albumId: albumId,
                        artistId: artistId
                    });
                },
                setRating: function(id, rating) {
                    return call('setRating.view', {
                        id: id,
                        rating: rating
                    });
                },
                scrobble: function(id, time, submission) {
                    return call('scrobble.view', {
                        id: id,
                        time: time,
                        submission: submission
                    });
                },
                getShares: function() {
                    return call('getShares.view');
                },
                createShare: function(id, description, expires) {
                    return call('createShare.view', {
                        id: id,
                        description: description,
                        expires: expires
                    })
                },
                updateShare: function(id, description, expires) {
                    return call('updateShare.view', {
                        id: id,
                        description: description,
                        expires: expires
                    })
                },
                deleteShare: function(id) {
                    return call('deleteShare.view', {
                        id: id
                    })
                },
                getPodcasts: function(includeEpisodes, id) {
                    return call('getPodcasts.view', {
                        includeEpisodes: includeEpisodes,
                        id: id
                    });
                },
                refreshPodcasts: function() {
                    return call('refreshPodcasts.view');
                },
                createPodcastChannel: function(url) {
                    return call('createPodcastChannel.view', {
                        url: url
                    });
                },
                deletePodcastChannel: function(id) {
                    return call('deletePodcastChannel.view', {
                        id: id
                    });
                },
                deletePodcastEpisode: function(id) {
                    return call('deletePodcastEpisode.view', {
                        id: id
                    });
                },
                downloadPodcastEpisode: function(id) {
                    return call('downloadPodcastEpisode.view', {
                        id: id
                    });
                },
                jukeboxControl: function(action, index, offset, id, gain) {
                    return call('jukeboxControl.view', {
                        action: action,
                        index: index,
                        offset: offset,
                        id: id,
                        gain: gain
                    });
                },
                getInternetRadioStations: function() {
                    return call('getInternetRadioStations.view');
                },
                getChatMessages: function(since) {
                    return call('getChatMessages.view', {
                        since: since
                    });
                },
                addChatMessage: function(message) {
                    return call('addChatMessage.view', {
                        message: message
                    });
                },
                getUser: function(username) {
                    return call('getUser.view', {
                        username: username
                    });
                },
                getUsers: function() {
                    return call('getUsers.view');
                },
                createUser: function(username, password, email, ldapAuthenticated, adminRole, settingsRole, streamRole, jukeboxRole, downloadRole, uploadRole, playlistRole, coverArtRole, commentRole, podcastRole, shareRole, musicFolderId) {
                    return call('createUser.view', {
                        username: username,
                        password: password,
                        email: email,
                        ldapAuthenticated: ldapAuthenticated,
                        adminRole: adminRole,
                        settingsRole: settingsRole,
                        streamRole: streamRole,
                        jukeboxRole: jukeboxRole,
                        downloadRole: downloadRole,
                        uploadRole: uploadRole,
                        playlistRole: playlistRole,
                        coverArtRole: coverArtRole,
                        commentRole: commentRole,
                        podcastRole: podcastRole,
                        shareRole: shareRole,
                        musicFolderId: musicFolderId
                    })
                },
                updateUser: function(username, password, email, ldapAuthenticated, adminRole, settingsRole, streamRole, jukeboxRole, downloadRole, uploadRole, playlistRole, coverArtRole, commentRole, podcastRole, shareRole, musicFolderId) {
                    return call('updateUser.view', {
                        username: username,
                        password: password,
                        email: email,
                        ldapAuthenticated: ldapAuthenticated,
                        adminRole: adminRole,
                        settingsRole: settingsRole,
                        streamRole: streamRole,
                        jukeboxRole: jukeboxRole,
                        downloadRole: downloadRole,
                        uploadRole: uploadRole,
                        playlistRole: playlistRole,
                        coverArtRole: coverArtRole,
                        commentRole: commentRole,
                        podcastRole: podcastRole,
                        shareRole: shareRole,
                        musicFolderId: musicFolderId
                    })
                },
                deleteUser: function(username) {
                    return call('deleteUser.view', {
                        username: username
                    });
                },
                changePassword: function(username, password) {
                    return call('changePassword.view', {
                        username: username,
                        password: password
                    });
                },
                getBookmarks: function() {
                    return call('getBookmarks.view');
                },
                createBookmark: function(id, position, comment) {
                    return call('createBookmark.view', {
                        id: id,
                        position: position,
                        comment: comment
                    });
                },
                deleteBookmark: function(id) {
                    return call('deleteBookmark.view', {
                        id: id
                    });
                },
                getPlayQueue: function() {
                    return call('getPlayQueue.view');
                },
                savePlayQueue: function(id, current, position) {
                    return call('savePlayQueue.view', {
                        id: id,
                        current: current,
                        position: position
                    });
                },
            };
        }
    ]);


    app.factory('PlayerSrvc', [
        '$filter',
        'SubsonicAPI',
        function(
            $filter,
            SubsonicAPI
        ) {
            return {
                queue: [],
                idx: 0,
                paused: true,
                volume: 100,
                setVolume: function(volume) {
                    this.volume = volume;

                    if (this.getSong()) {
                        this.getSong().sound.setVolume(this.volume);
                    }
                },
                getSong: function() {
                    return this.queue[this.idx];
                },
                getPercent: function() {
                    var song = this.getSong();
                    if (song && song.sound.getTime()) {
                        return song.sound.getTime() / song.sound.getDuration() * 100;
                    }
                    return 0;
                },
                play: function() {
                    this.paused = false;
                    this.getSong().sound.play();
                },
                pause: function() {
                    this.paused = true;
                    this.getSong().sound.pause();
                },
                togglePlay: function() {
                    if (this.paused) {
                        this.play();
                    } else {
                        this.pause();
                    }
                },
                skipTo: function(idx) {
                    if (angular.isNumber(idx) && this.idx !== idx) {
                        if (! this.paused) {
                            this.getSong().sound.stop();
                        }

                        this.idx = idx;
                        this.play();
                    }
                },
                next: function() {
                    if (this.idx + 1 < this.queue.length) {
                        // if a song is currently playing, stop it first
                        if (! this.paused) {
                            this.getSong().sound.stop();
                        }

                        // increment the index
                        this.idx += 1;

                        if (! this.paused) {
                            this.play();
                        }
                    }
                },
                previous: function() {
                    if (this.idx - 1 >= 0) {
                        if (! this.paused) {
                            this.getSong().sound.stop();
                        }

                        this.idx -= 1;

                        if (! this.paused) {
                            this.play();
                        }
                    }
                },
                add: function(song, idx) {
                    // make a copy to break the reference
                    var copy = {},
                        that = this;
                    angular.copy(song, copy);

                    // create a sound object
                    copy.sound = new buzz.sound(SubsonicAPI.stream(copy.id));
                    copy.sound.bind('play', function(e) {
                        this.setVolume(that.volume);
                    });

                    if (angular.isNumber(idx)) {
                        // if an index was supplied, add it at that position
                        this.queue.splice(idx, 0, copy);
                    } else {
                        // no index supplied, append on to end
                        this.queue.push(copy);
                    }
                },
                remove: function(item) {
                    if (angular.isNumber(item)) {
                        // if number was given, remove from that position
                        this.queue.splice(item, 1);
                    } else if (item.id) {
                        // if object was given, remove all that match the id
                        this.queue = $filter('filter')(this.queue, {id: '!' + item.id});
                    }
                }
            }
        }
    ]);

})(angular, jQuery);
